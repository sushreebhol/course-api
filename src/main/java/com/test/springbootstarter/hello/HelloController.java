package com.test.springbootstarter.hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//Request can be mapped to a particular method
@RestController
public class HelloController {
	
	//url to map the particular method
	@RequestMapping("/hello")
	public String sayHi()
	{
		return "hi";
	}

}
